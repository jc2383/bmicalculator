package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculateBMIPressed(View view) {
        // 1. Get the user input (height, weight)
        // ---------------------------
        // Edit Text variables
        EditText heightTextBox = (EditText) findViewById(R.id.editTextHeight);
        EditText weightTextBox = (EditText) findViewById(R.id.editTextWeight);

        // get the height value from the text box & convert it to a string
        String heightInput = heightTextBox.getText().toString();
        double height = Double.parseDouble(heightInput);

        String weightInput = weightTextBox.getText().toString();
        double weight = Double.parseDouble(weightInput);

        // ----------------------
        // 2. Calculate the BMI
        // bmi = weight / (height * height)
        // ----------------------
        double bmi = weight / (height * height);

        // 3. Categorize how fat the person is
        String status = "";
        if (bmi < 18.5) {
            status = "underweight";
        }
        else if (bmi >= 18.5 && bmi <= 24.9) {
            status = "normal";
        }
        else if (bmi >= 25 & bmi <= 29.9) {
            status = "overweight";
        }
        else {
            status = "obese";
        }



        // 4. Display the result
        TextView resultsLabel = (TextView) findViewById(R.id.textViewResults);
        resultsLabel.setText("You are " + status);



    }


}
